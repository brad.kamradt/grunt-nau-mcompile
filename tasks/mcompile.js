/*
 * grunt-mcompile
 * https://github.com/JohnCashBB/grunt-mcompile
 *
 * Copyright (c) 2013 John Cashmore
 * Licensed under the MIT license.
 */
'use strict';

module.exports = function (grunt) {

	// Please see the Grunt documentation for more information regarding task
	// creation: http://gruntjs.com/creating-tasks

	grunt.registerMultiTask('mcompile', 'Your task description goes here.', function () {
		// Merge task-specific and/or target-specific options with these defaults.
		var options = this.options({
			templateRoot: '',
			partialsRoot: '',
			dataRoot: '',
			filename: 'index',
		});
		var mustache = require('mustache'),
			error = false,
			partials = {};
		// Iterate over all specified file groups.

		for (var key in options.partialsArray) {
			partials[key] = grunt.file.read(options.partialsRoot + options.partialsArray[key]);
		}

		this.files.forEach(function(file) {
			grunt.log.write(file.dest);
			file.src.forEach(function(filename) {
				grunt.log.write(filename);

				var jsonPath = 'data/' + file.dest;
				var jsonFile = jsonPath.replace('.html', '.json');

				var template = grunt.file.read(filename);
				var view = grunt.file.readJSON(jsonFile);

				var data = mustache.render(template, view, partials);

				grunt.file.write(file.dest, data);
				//grunt.file.write('dest/' + options.filename + '.html', data);

			});

		});

	
	});

};
